// notification.dto.ts

export class CreateNotificationDto {
     notification_title: string;
     notification_description: string;
     url_img?: string;
     receptor?: number;
     idGame?: number;
     tokens?: string[];
     condition?: string;

     constructor(
          notification_title: string,
          notification_description: string,
          receptor: number,
          url_img?: string,
          idGame?: number,
          tokens?: string[],
          condition?: string
     ) {
          this.notification_title = notification_title;
          this.notification_description = notification_description;
          this.receptor = receptor;
          this.url_img = url_img;
          this.idGame = idGame;
          this.tokens = tokens;
          this.condition = condition;
     }
}

export class SubscribeDto {
     tokens: string[];
     topic?: string;

     constructor(token: string[], topic: string) {
          this.tokens = token;
          this.topic = topic;
     }
}
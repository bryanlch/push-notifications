import * as admin from 'firebase-admin';
import { ConfigService } from '@nestjs/config';
import * as serviceAccount from './../../../keys/sendo-notification-64bff-b964140b23dd.json';

export const firebaseConfigProvider = {
     provide: 'FIREBASE_ADMIN',
     inject: [ConfigService],
     useFactory: () => {
          return admin.initializeApp({
               credential: admin.credential.cert(serviceAccount as admin.ServiceAccount),
          });
     },
};

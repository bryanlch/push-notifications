import { Body, Controller, Get, Inject, Post, Res } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { CreateNotificationDto, SubscribeDto } from 'src/common/dto/create-notification.dto';
import { ClientProxy } from '@nestjs/microservices';

const STATUS_CODES = {
     OK: 200,
     CREATED: 201,
     BAD_REQUEST: 400,
     INTERNAL_SERVER_ERROR: 500,
};

const errorMappings = {
     'messaging/registration-token-not-registered': 'Registration token not registered',
};

@Controller('push')
export class NotificationController {
     constructor(
          private readonly notificationService: NotificationService,
          @Inject('KAFKA') private readonly kafkaConnect: ClientProxy,
     ) { }

     private async sendKafkaMessage(topic: string, message: any) {
          return this.kafkaConnect.emit(topic, message);
     }

     @Get('token')
     async createToken(@Res() res) {
          try {
               const response = await this.notificationService.createToken();
               return res.status(STATUS_CODES.OK).json(response);
          } catch (error) {
               return res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json(error);
          }
     }

     @Post('subscribe')
     async subscribeToken(@Res() res, @Body() token: SubscribeDto) {
          try {
               const response = await this.notificationService.firebaseSubscribeToken(token);
               return res.status(STATUS_CODES.OK).json(response);
          } catch (error) {
               console.log("🚀 ~ file: notification.controller.ts:37 ~ error:", error)
               return res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json(error);
          }
     }

     @Post('unsubscribe')
     async unsubscribeToken(@Res() res, @Body() token: SubscribeDto) {
          try {
               const response = await this.notificationService.firebaseUnsubscribeToken(token);
               return res.status(STATUS_CODES.OK).json(response);
          } catch (error) {
               return res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json(error);
          }
     }

     @Post('sendByToken')
     async sendNotificationToken(@Res() res, @Body() createNotificationDto: CreateNotificationDto) {
          try {
               let tryCount = 0;
               while (tryCount < 3) {
                    const response = await this.notificationService.sendMulticastNotification(createNotificationDto);
                    if (response?.success) {
                         return res.status(STATUS_CODES.OK).json(response);
                    }

                    if (!response?.success && errorMappings[response.error]) {
                         throw new Error(response?.error);
                    }
                    tryCount++;
               }
               throw new Error('Failed to send notification after 3 attempts');
          } catch (error) {
               return res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json({
                    message: 'Internal server error',
                    error: error.message
               });
          }
     }

     @Post('sendByTopic')
     async sendNotificationTopic(@Res() res, @Body() createNotificationDto: CreateNotificationDto) {
          try {
               let tryCount = 0;
               while (tryCount < 3) {
                    const response = await this.notificationService.sendTopicNotification(createNotificationDto);
                    if (response?.success) {
                         return res.status(STATUS_CODES.OK).json(response);
                    }

                    if (!response?.success && errorMappings[response.error]) {
                         throw new Error(response?.error);
                    }
                    tryCount++;
               }
               throw new Error('Failed to send notification after 3 attempts');
          } catch (error) {
               return res.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json({
                    message: 'Internal server error',
                    error: error.message
               });
          }
     }

}
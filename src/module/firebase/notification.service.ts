import { Injectable } from "@nestjs/common";
import { getMessaging } from "firebase-admin/messaging";
import { CreateNotificationDto, SubscribeDto } from "../../common/dto/create-notification.dto";
import * as serviceAccount from '../../config/firebase/keys/sendo-notification-64bff-b964140b23dd.json';
import { google } from 'googleapis';

@Injectable()
export class NotificationService {
     constructor() { }

     private manageError(error: any) {
          return {
               success: false,
               error: error
          };
     }

     async createToken() {
          try {
               const PRIVATE_KEY = serviceAccount.private_key.replace(/\\n/g, '\n');
               const CLIENT_ID = serviceAccount.client_id;
               const t = await new Promise(function (resolve, reject) {
                    var jwtClient = new google.auth.JWT(
                         CLIENT_ID,
                         null,
                         PRIVATE_KEY,
                         ['https://www.googleapis.com/auth/firebase.messaging'],
                         null
                    );

                    jwtClient.authorize(function (err, tokens) {
                         if (err) {
                              reject(err);
                              return;
                         }

                         resolve(tokens.access_token);
                    });
               });
               console.log("----------------------------------")
               console.log(t);
               return {
                    token: t,
                    success: true
               }
          } catch (error) {
               console.log("🚀 ~ error in firebase.service.ts:35 ~ ", error)
          }
     }

     async firebaseSubscribeToken(token: SubscribeDto) {
          try {
               const { tokens, topic = 'anonymous' } = token;
               return await getMessaging().subscribeToTopic(tokens, topic)
                    .then((response) => ({ success: true, error: null, response }))
                    .catch((error) => this.manageError(error));
          } catch (error) {
               console.log("🚀 ~ file: notification.service.ts:51 ~ error:", error)
               return this.manageError(error);
          }
     }

     async firebaseUnsubscribeToken(token: SubscribeDto) {
          try {
               const { tokens, topic = 'anonymous' } = token;
               return await getMessaging().unsubscribeFromTopic(tokens, topic)
                    .then((response) => ({ success: true, error: null, response }))
                    .catch((error) => this.manageError(error));
          } catch (error) {
               console.log("🚀 ~ file: notification.service.ts:63 ~ error:", error)
               return this.manageError(error);
          }
     }

     async sendTopicNotification(createNotificationDto: CreateNotificationDto) {
          try {
               const receptor = this.getReceptor(createNotificationDto.receptor) ?? 0;
               const condition: string = this.getCondition(receptor, createNotificationDto.condition ?? '');

               const dataToSend: any = this.constructNotificationData(createNotificationDto);
               dataToSend.condition = condition;

               const response = await getMessaging().send(dataToSend);
               console.log("🚀 ~ response:", response)
               return {
                    success: true,
                    error: null
               };
          } catch (error) {
               console.error("🚀 ~ sendTopicNotification in firebase.service.ts:73 ~", error)
               return this.manageError(error);
          }
     }

     async sendMulticastNotification(createNotificationDto: CreateNotificationDto) {
          try {
               let isSuccess: boolean = false;
               let error: any = null;

               if (createNotificationDto.tokens.length === 0) {
                    return this.manageError('No tokens to send');
               }

               const dataToSend: any = this.constructNotificationData(createNotificationDto);
               dataToSend.tokens = createNotificationDto?.tokens ?? [];

               const response = await getMessaging().sendMulticast(dataToSend);
               console.log("🚀 ~ response:", response?.responses[0]?.error?.code);

               isSuccess = response?.responses[0]?.success;
               error = response?.responses[0]?.error?.code ?? null;

               return {
                    success: isSuccess,
                    error: error
               }
          } catch (err) {
               console.log("🚀 ~ sendMulticastNotification in firebase.service.ts ~ ", err)
               return this.manageError(err);
          }
     }

     private constructNotificationData(createNotificationDto: CreateNotificationDto): any {
          return {
               "notification": {
                    "title": createNotificationDto?.notification_title ?? '',
                    "body": createNotificationDto?.notification_description ?? '',
                    "image": createNotificationDto?.url_img ?? '',
               },
               "data": {
                    "click_action": "FLUTTER_NOTIFICATION_CLICK",
                    "title": createNotificationDto?.notification_title ?? '',
                    "body": createNotificationDto?.notification_description ?? '',
                    "image": createNotificationDto?.url_img ?? '',
                    "content_available": 'true',
                    "idGame": createNotificationDto?.idGame ? createNotificationDto.idGame.toString() : ""
               },
          };
     }

     private getCondition(receptor: number, condition: string): string {

          if (condition) {
               return `\'${condition}\' in topics`;
          }

          switch (receptor) {
               case 1:
                    return '\'anonymous\' in topics || \'registered\' in topics';
               case 2:
                    return '\'anonymous\' in topics';
               case 3:
                    return '\'registered\' in topics';
               default:
                    return '\'anonymous\' in topics';
          }
     }

     private getReceptor(receptor: any): number {
          try {
               if (typeof receptor === 'string') {
                    const receptorNumber = parseInt(receptor);
                    return receptorNumber ?? 0;
               }
               return receptor ?? 0;
          } catch (error) {
               console.error("🚀 ~ error: notification.service.ts:143 ~", error)
               return 0;
          }
     }

}
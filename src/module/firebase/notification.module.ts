import { Module } from '@nestjs/common';
import { NotificationService } from './notification.service';
import { NotificationController } from './notification.controller';

import { firebaseConfigProvider } from 'src/config/firebase/firebase.config';
import { ConfigModule } from '@nestjs/config';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
     imports: [
          ConfigModule,
          ClientsModule.register([
               {
                    name: 'KAFKA',
                    transport: Transport.KAFKA,
                    options: {
                         client: {
                              brokers: [process.env.KAFKA_BROKERS],
                         },

                    }
               },
          ]),
     ],
     providers: [
          firebaseConfigProvider,
          NotificationService,
     ],
     exports: [NotificationService],
     controllers: [NotificationController],
})

export class NotificationeModule { } 
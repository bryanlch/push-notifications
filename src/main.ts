import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { ConfigService } from '@nestjs/config';
import { CorsOptions } from '@nestjs/common/interfaces/external/cors-options.interface';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const configService = app.get(ConfigService);
  const port = configService.get('PORT');
  const brokers = configService.get('KAFKA_BROKERS');

  app.setGlobalPrefix('api');

  app.connectMicroservice({
    transport: Transport.KAFKA,
    options: {
      subscribe: {
        fromBeginning: true,
      },
      consumer: {
        groupId: 'kafka-notification-consumer'
      },
      client: {
        brokers: [brokers],
      },
    },
  } as MicroserviceOptions);

  const corsOptions: CorsOptions = {
    origin: 'http://localhost:3016',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204,
    credentials: true,
    allowedHeaders: 'Origin,X-Requested-With,Content-Type,Accept,Authorization',
  };
  app.enableCors(corsOptions);

  app.startAllMicroservices();

  await app.listen(port);
}
bootstrap();
